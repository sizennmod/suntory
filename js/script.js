/*
   script.js
   (c) sunlogi
*/

;(function($) { // 末尾にセミコロンが無いファイルと結合した場合の予防に行頭にセミコロンを書く
  // "usr strict"; // ストリクト(厳格)モードをオンにする
  $(function() { // $().ready のショートハンド / DOMの準備ができたら実行される
	// ----------------
	// init
	// IE link dot
	// a:hover img opacity
	// a img on off
	// smooth scroll
	// window center
	// first_view_fix
	// first_view btn click
	// scroll fix
	// scroll delay
	// ----------------
	// init
	var ulw;
	var lih;
	var w;
	var h;
	var $body = $("body");


	//IE link dot
	$("a").focus(function(){this.blur();});
	// a > div
	$(".box_links").click(function(){
		if($(this).find("a").attr("target")=="_blank"){
			window.open($(this).find("a").attr("href"), "_blank");
		}else{
			window.location=$(this).find("a").attr("href");
		}
		return false;
	});
	// a:hover img opacity
	$("a img.img_over").hover(function(){
		$(this).fadeTo("1000", 0.5); // opacity 50
	},function(){
		$(this).fadeTo("1000", 1.0); // opacity 100
	});
	$("img.img_opacity").hover(function(){
		$(this).fadeTo("1000", 0.5); // opacity 50
	},function(){
		$(this).fadeTo("1000", 1.0); // opacity 100
	});
	// a img on off
	$('a img.switch').hover(function(){
		$(this).attr('src', $(this).attr('src').replace('_off', '_on'));
		$(this).attr('srcset', $(this).attr('srcset').replace('_off', '_on'));
		$(this).attr('srcset', $(this).attr('srcset').replace('_off', '_on'));
			}, function(){
			   if (!$(this).hasClass('current')) {
			   $(this).attr('src', $(this).attr('src').replace('_on', '_off'));
			   $(this).attr('srcset', $(this).attr('srcset').replace('_on', '_off'));
			   $(this).attr('srcset', $(this).attr('srcset').replace('_on', '_off'));
		}
	});

	// loadしたら
	$(window).on("load", function(){
		w = $(window).width();
		h = $(window).height();
		console.log("call");
		if(w <= 768){ // 768 min
			console.log("calls");
			h = window.innerHeight;
			sp_nav(w,h);
		}else{ // 768 over
		}
	});
	$(window).on("load resize", function(){
		w = $(window).width();
		h = $(window).height();
		// first_view_fix(w,h);
	 });

	function sp_nav(){
		$(".js-navBody").css({
			'width' : w,
			'height' : h
		});
		$(".js-navBtn").click(function(){
			console.log("click");
			$(this).next(".js-navBody").slideToggle();
			$(this).toggleClass("is_open");
		});
	}
  });
})(window.jQuery);
